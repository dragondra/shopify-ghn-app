require('isomorphic-fetch');
require('dotenv').config();

import fs from 'fs';
import express from 'express';
import session from 'express-session';
const RedisStore = require('connect-redis')(session);
import path from 'path';
import axios from 'axios';
import logger from 'morgan';
// import webpack from 'webpack';
// import webpackMiddleware from 'webpack-dev-middleware';
// import webpackHotMiddleware from 'webpack-hot-middleware';
import config from '../config/webpack.config.js';
import ShopifyAPIClient from 'shopify-api-node';
import ShopifyExpress from '@shopify/shopify-express';
import { RedisStrategy } from '@shopify/shopify-express/strategies';

const {
  SHOPIFY_APP_KEY,
  SHOPIFY_APP_HOST,
  SHOPIFY_APP_SECRET,
  SHOPIFY_APP_SCOPE,
  NODE_ENV,
} = process.env;

const databaseConfig = {
  client: 'pg',
  connection: {
    host: '127.0.0.1',
    user: 'postgres',
    password: 'admin',
    database: 'bitcode-ghn',
  },
};

const shopifyConfig = {
  host: SHOPIFY_APP_HOST,
  apiKey: SHOPIFY_APP_KEY,
  secret: SHOPIFY_APP_SECRET,
  scope: [SHOPIFY_APP_SCOPE],
  shopStore: new RedisStrategy(),
  afterAuth(request, response) {
    const {
      session: { accessToken, shop },
    } = request;

    registerWebhook(shop, accessToken, {
      topic: 'orders/create',
      address: `${SHOPIFY_APP_HOST}/order-create`,
      format: 'json',
    });

    registerWebhook(shop, accessToken, {
      topic: 'fulfillments/create',
      address: `${SHOPIFY_APP_HOST}/fulfillments-create`,
      format: 'json',
    });

    registerCarrierService(shop, accessToken, {
      name: '#[thien] GHN',
      callback_url: `${SHOPIFY_APP_HOST}/carrier-service`,
      service_discovery: true,
    });

    registerScriptTag(shop, accessToken, {
      event: 'onload',
      src: `${SHOPIFY_APP_HOST}/assets/script/carrier-service.js`,
      display_scope: 'online_store',
    });

    return response.redirect('/');
  },
};

const registerWebhook = (shopDomain, accessToken, webhook) => {
  const shopify = new ShopifyAPIClient({
    shopName: shopDomain,
    accessToken,
  });
  shopify.webhook
    .create(webhook)
    .then(
      response => console.log(`webhook '${webhook.topic}' created`),
      err =>
        console.log(
          `Error creating webhook '${
            webhook.topic
          }'. ${JSON.stringify(err.response.body)}`,
        ),
    );
};

const registerCarrierService = (
  shopDomain,
  accessToken,
  carrierService,
) => {
  const shopify = new ShopifyAPIClient({
    shopName: shopDomain,
    accessToken,
  });
  shopify.carrierService
    .create(carrierService)
    .then(
      response =>
        console.log(
          `carrierService '${carrierService.name}' created`,
        ),
      err =>
        console.log(
          `Error creating carrierService '${
            carrierService.topic
          }'. ${JSON.stringify(err.response.body)}`,
        ),
    );
};

const registerScriptTag = (shopDomain, accessToken, scriptTag) => {
  const shopify = new ShopifyAPIClient({
    shopName: shopDomain,
    accessToken,
  });
  shopify.scriptTag
    .create(scriptTag)
    .then(
      response => console.log(`scriptTag '${scriptTag.src}' created`),
      err =>
        console.log(
          `Error creating scriptTag '${
            scriptTag.topic
          }'. ${JSON.stringify(err.response.body)}`,
        ),
    );
};

const app = express();
const isDevelopment = NODE_ENV !== 'production';

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(
  session({
    store: isDevelopment ? undefined : new RedisStore(),
    secret: SHOPIFY_APP_SECRET,
    resave: true,
    saveUninitialized: false,
  }),
);

// Run webpack hot reloading in dev
// if (isDevelopment) {
//   const compiler = webpack(config);
//   const middleware = webpackMiddleware(compiler, {
//     hot: true,
//     inline: true,
//     publicPath: config.output.publicPath,
//     contentBase: 'src',
//     stats: {
//       colors: true,
//       hash: false,
//       timings: true,
//       chunks: false,
//       chunkModules: false,
//       modules: false,
//     },
//   });

//   app.use(middleware);
//   app.use(webpackHotMiddleware(compiler));
// } else {
//   const staticPath = path.resolve(__dirname, '../assets');
//   app.use('/assets', express.static(staticPath));
// }

const staticPath = path.resolve(__dirname, '../assets');
app.use('/assets', express.static(staticPath));

// Install
app.get('/install', (req, res) => res.render('install'));

// Create shopify middlewares and router
const shopify = ShopifyExpress(shopifyConfig);

// Mount Shopify Routes
const { routes, middleware } = shopify;
const { withShop, withWebhook } = middleware;

app.use('/shopify', routes);

// Client
app.get(
  '/',
  withShop({ authBaseUrl: '/shopify' }),
  (request, response) => {
    const {
      session: { shop, accessToken },
    } = request;
    response.render('app', {
      title: 'Shopify Node App',
      apiKey: shopifyConfig.apiKey,
      shop,
    });
  },
);

app.post(
  '/order-create',
  withWebhook((error, request) => {
    if (error) {
      console.error(error);
      return;
    }

    console.log('We got a webhook order create!');
    console.log('Details: ', request.webhook);
    console.log('Body:', request.body);
  }),
);

app.post(
  '/fulfillments-create',
  withWebhook(async (error, request) => {
    if (error) {
      console.error(error);
      return;
    }
    const body = {
      tracking_url: 'thien.com',
    };

    const reqBody = JSON.parse(request.body);

    // await axios
    //   .put(
    //     `https://${request.webhook.shopDomain}/admin/orders/${
    //       reqBody.order_id
    //     }/fulfillments/${reqBody.id}.json`,
    //     body,
    //     {
    //       header: {
    //         'X-Shopify-Access-Token': request.webhook.accessToken,
    //       },
    //     },
    //   )
    //   .then(responseFulfill => {
    //     console.log('update fulfill success');
    //     console.log(responseFulfill.data);
    //   })
    //   .catch(err => {
    //     console.log(`Error update fulfill . ${err}`);
    //   });

    const shopify = new ShopifyAPIClient({
      shopName: request.webhook.shopDomain,
      accessToken: request.webhook.accessToken,
    });
    shopify.fulfillment
      .update(reqBody.order_id, reqBody.id, body)
      .then(
        responseFulfill => console.log('update fulfill success'),
        err =>
          console.log(
            `Error update fulfill . ${JSON.stringify(
              err.response.body,
            )}`,
          ),
      );
  }),
);

app.post('/carrier-service', (request, response) => {
  console.log('We got a webhook carrier service!');
  console.log('Rate: ', request.rate);

  response.status(200).json({
    rates: [
      {
        service_name: '#[thien] GHN',
        service_code: 'ON',
        total_price: '1295',
        description: 'This is the fastest option by far',
        currency: 'VND',
        phone_required: true,
      },
    ],
  });
});

app.get(
  '/list-order',
  withShop({ authBaseUrl: '/shopify' }),
  (request, response) => {
    const {
      session: { shop, accessToken },
    } = request;
    const shopify = new ShopifyAPIClient({
      shopName: shop,
      accessToken,
    });
    shopify.order.list().then(responseOrder => {
      response.json(responseOrder);
    });
  },
);

// Error Handlers
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use((error, request, response, next) => {
  response.locals.message = error.message;
  response.locals.error =
    request.app.get('env') === 'development' ? error : {};

  response.status(error.status || 500);
  response.render('error');
});

export default app;
